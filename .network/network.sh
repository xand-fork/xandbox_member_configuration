#!/bin/bash
# TODO ADO 6558 - move to own project + refactor + generalize + publish docker image for CIs in other projects

# SHELL OPTIONS
set -o errexit # abort on nonzero exitstatus
set -o nounset # abort on unbound variable

# SELF
DIR="$( dirname "${BASH_SOURCE[0]}" )"
ABS_PATH_DIR="$(realpath ${DIR})"

# IN
CONFIG_TEMPLATE="${ABS_PATH_DIR}/config.template.yaml"
ENV_CONFIG="${ABS_PATH_DIR}/.env"
AUTH_ENV_CONFIG="${ABS_PATH_DIR}/.auth.env"

# OUT
OUT_DIR=${ABS_PATH_DIR}
OUT_NAME="network"
OUT=${OUT_DIR}/${OUT_NAME}

# TEMP
TMP="${ABS_PATH_DIR}/tmp"

# HELP PRINTOUT
print_help() {
  HELPTEXT="$(cat << END
    Help:

    This script generates a network via XNG using variables listed in .env and the configuration template config.template.yaml.

    Arguments
      -u (Required) = Artifactory Username
      -p (Required) = Artifactory Password

    Options
      -h: Print help
      -a: Generate a network using authentication
      -i: Install the proper version of XNG during script run
      -r: Run the generated network after script run
END
  )"
  echo -e "$HELPTEXT"
}

# CREDENTIALS & FLAGS
while getopts u:p:airh opt
do
    case "${opt}" in
        u) ARTIFACTORY_USERNAME=${OPTARG};;
        p) ARTIFACTORY_PASSWORD=${OPTARG};;
        a) USE_AUTH=1;;
        i) INSTALL_XNG=1;;
        r) RUN_NETWORK=1;;
        h) print_help && exit ;;
        *) print_help && exit 1;;
    esac
done

clean_build_dir() {
  rm -rf ${OUT}
}

create_temp_dir() {
  mkdir -p ${TMP}
}

load_vars() {
  if [ ${USE_AUTH:-0} -eq 1 ]; then source ${AUTH_ENV_CONFIG}; else source ${ENV_CONFIG}; fi
}

generate_config() {
  cp ${CONFIG_TEMPLATE} ${TMP}/config.yaml

  declare -A replacements

  replacements[MEMBER_API_IMAGE]=${MEMBER_API_IMAGE}
  replacements[VALIDATOR_IMAGE]=${VALIDATOR_IMAGE}
  replacements[TRUST_IMAGE]=${TRUST_IMAGE}
  replacements[BANK_MOCKS_IMAGE]=${BANK_MOCKS_IMAGE}
  replacements[TRUST_API_IMAGE]=${TRUST_API_IMAGE}
  replacements[MINOR_UNITS_PER_VALIDATOR_EMISSION]=${MINOR_UNITS_PER_VALIDATOR_EMISSION}
  replacements[BLOCK_QUOTA]=${BLOCK_QUOTA}
  replacements[VALIDATOR_COUNT]=${VALIDATOR_COUNT}
  replacements[MEMBER_COUNT]=${MEMBER_COUNT}
  replacements[ENABLE_AUTH]=${ENABLE_AUTH}

  echo -e "Creating XNG Config"
  for k in "${!replacements[@]}"
    do
        echo -e "\tReplacing ${k} with ${replacements[${k}]}"
        sed -i -e "s@\${${k}}@${replacements[${k}]}@" ${TMP}/config.yaml
    done
}

install_xng() {
    if [ ${INSTALL_XNG:-0} -eq 1 ]; then cargo install xand_network_generator --registry tpfs --version ${XAND_NETWORK_GENERATOR_VERSION} -f; fi
}

generate_xandbox_network() {
  zip_filename="chain-spec-template.${CHAINSPEC_VERSION}.zip"

  wget --directory-prefix=${TMP} --user=${ARTIFACTORY_USERNAME}  --password=${ARTIFACTORY_PASSWORD} \
    https://transparentinc.jfrog.io/artifactory/artifacts-internal/chain-specs/templates/${zip_filename}

  xand_network_generator generate  \
    --config ${TMP}/config.yaml \
    --chainspec-zip  ${TMP}/${zip_filename} \
    --output-dir ${OUT}
}

run_network() {
    if [ ${RUN_NETWORK:-0} -eq 1 ]; then (cd ${OUT} && docker-compose up -d); fi
}

delete_temp_dir() {
    rm -rf ${TMP}
}

clean_build_dir
create_temp_dir
load_vars
install_xng
generate_config
generate_xandbox_network
run_network
delete_temp_dir
